﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;

namespace members
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private SQLiteConnection sql_con;
        private SQLiteCommand sql_cmd;
        private SQLiteDataAdapter DB;
        private DataSet DS = new DataSet();
        private DataTable  DT = new DataTable();

        private void Form1_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        //Set connection
        private void SetConnection()
        {
            sql_con = new SQLiteConnection("Data Source=bdMember.db;Version=3;New=False;Compress=True;");
        }

        //Set executequery
        private void ExecuteQuery(String txtQuery)
        {
            SetConnection();
            sql_con.Open();
            sql_cmd = sql_con.CreateCommand();
            sql_cmd.CommandText = txtQuery;
            sql_cmd.ExecuteNonQuery();
            sql_con.Close();
        }
        //Set LoadDB
        private void LoadData()
        {
            SetConnection();
            sql_con.Open();
            sql_cmd = sql_con.CreateCommand();
            string CommandText = "select * from users";
            DB = new SQLiteDataAdapter(CommandText, sql_con);
            DS.Reset();
            DB.Fill(DS);
            DT = DS.Tables[0];
            dataGridView1.DataSource = DT;
            sql_con.Close();

        }

        //clear textbox
        private void clearTb()
        {
            tbID.Clear();
            tbName.Clear();
            tbLname.Clear();
            tbPhone.Clear();
            tbMail.Clear();
            tbNotes.Clear();
            dtpB.ResetText();
            dtpW.ResetText();
            cbbM.ResetText();
        }

        //add
        private void btnAdd_Click(object sender, EventArgs e)
        {
            string txtQuery = "insert into users (ID,Name,Last_Name,Phone,Mail,Notes,Date_of_Birth,Weeding_date,Member_type)" +
                "values('"+tbID.Text+"','"+tbName.Text+"','"+tbLname.Text+"','"+tbPhone.Text+"','"+tbMail.Text+"','"+tbNotes.Text+"','"+dtpB.Text+"','"+dtpW.Text+"','"+cbbM.Text+"')";
            ExecuteQuery(txtQuery);
            LoadData();
            clearTb();
            MessageBox.Show("Added member!");
        }
        //edit
        private void btnEdt_Click(object sender, EventArgs e)
        {
            MessageBoxButtons botones = MessageBoxButtons.YesNo;
            DialogResult dr = MessageBox.Show("Are you sure you want to update the data?", "warning!", botones, MessageBoxIcon.Question);
            if (dr == DialogResult.Yes)
            {
                string txtQuery = "update users set Name='" + tbName.Text + "',Last_Name='" + tbLname.Text + "',Phone='" + tbPhone.Text + "',Mail='" + tbMail.Text + "',Notes='" + tbNotes.Text + "',Date_of_Birth='" + dtpB.Text + "',Weeding_date='" + dtpW.Text + "',Member_type='" + cbbM.Text + "' where ID = '" + tbID.Text + "'";
                ExecuteQuery(txtQuery);
            }
            LoadData();
            clearTb();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            tbID.Text = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
            tbName.Text = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
            tbLname.Text = dataGridView1.SelectedRows[0].Cells[2].Value.ToString();
            tbPhone.Text = dataGridView1.SelectedRows[0].Cells[3].Value.ToString();
            tbMail.Text = dataGridView1.SelectedRows[0].Cells[4].Value.ToString();
            tbNotes.Text = dataGridView1.SelectedRows[0].Cells[5].Value.ToString();
            dtpB.Text = dataGridView1.SelectedRows[0].Cells[6].Value.ToString();
            dtpW.Text = dataGridView1.SelectedRows[0].Cells[7].Value.ToString();
            cbbM.Text = dataGridView1.SelectedRows[0].Cells[8].Value.ToString();
        }

        //delete
        private void btnDelete_Click(object sender, EventArgs e)
        {
            MessageBoxButtons botones = MessageBoxButtons.YesNo;
            DialogResult dr = MessageBox.Show("are you sure you want to delete the data?", "warning!", botones, MessageBoxIcon.Warning);
            if (dr == DialogResult.Yes)
            {
                string txtQuery = "delete from users  where ID = '" + tbID.Text + "'";
                ExecuteQuery(txtQuery);
            }
            LoadData();
            clearTb();
        }

        //Search
        private void btnSearch_Click(object sender, EventArgs e)
        {
            {
                string txtQuery = "select * from users where id = '" + tbSearch.Text + "'";
                txtQuery += " or ID = '" + tbSearch.Text + "' ";
                txtQuery += " or Name = '" + tbSearch.Text + "' ";
                txtQuery += " or Last_Name = '" + tbSearch.Text + "' ";
                txtQuery += " or Phone = '" + tbSearch.Text + "' ";
                txtQuery += " or Mail = '" + tbSearch.Text + "' ";
                txtQuery += " or Notes = '" + tbSearch.Text + "' ";
                txtQuery += " or Date_of_Birth = '" + tbSearch.Text + "' ";
                txtQuery += " or Weeding_date = '" + tbSearch.Text + "' ";
                txtQuery += " or Member_type = '" + tbSearch.Text + "' ";
                ExecuteQuery(txtQuery);
                DB = new SQLiteDataAdapter(txtQuery, sql_con);
                DS.Reset();
                DB.Fill(DS);
                DT = DS.Tables[0];
                dataGridView1.DataSource = DT;
                sql_con.Close();
                tbSearch.Clear();
                clearTb();
            }
        }

        private void btnAll_Click(object sender, EventArgs e)
        {
            LoadData();
            clearTb();
        }

        public void ExportarDatos(DataGridView datalistado)
        {
            Microsoft.Office.Interop.Excel.Application exportarexel = new Microsoft.Office.Interop.Excel.Application();

            exportarexel.Application.Workbooks.Add(true);

            int indicecolumna = 0;

            foreach (DataGridViewColumn columna in datalistado.Columns)
            {
                indicecolumna++;

                exportarexel.Cells[1, indicecolumna] = columna.Name;
            }

            int indicefila = 0;

            foreach (DataGridViewRow fila in datalistado.Rows)
            {
                indicefila++;
                indicecolumna = 0;
                foreach (DataGridViewColumn columna in datalistado.Columns)
                {
                    indicecolumna++;
                    exportarexel.Cells[indicefila + 1, indicecolumna] = fila.Cells[columna.Name].Value;
                }
            }

            exportarexel.Visible = true;

        }

        private void btnExcel_Click(object sender, EventArgs e)
        {
            ExportarDatos(dataGridView1);
        }

        private void dtpW_ValueChanged(object sender, EventArgs e)
        {
            dtpW.CustomFormat = "dd/MM/yyyy";
        }

        private void dtpW_KeyDown(object sender, KeyEventArgs e)
        {
            if((e.KeyCode==Keys.Back) || (e.KeyCode == Keys.Delete))
            {
                dtpW.CustomFormat = " ";
            }
        }
    }
}
